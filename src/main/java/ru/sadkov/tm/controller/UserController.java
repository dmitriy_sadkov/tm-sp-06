package ru.sadkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.User;
import ru.sadkov.tm.service.IUserService;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("/registration")
    public String registrationForm(Model model) {
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(Model model, @ModelAttribute("username") String login, @ModelAttribute("password") String password) {
        @NotNull final User user = userService.createUser(login, password);
        model.addAttribute("user", user);
        userService.userRegister(user);
        return "redirect:/";
    }

    @GetMapping("/profile/{login}")
    public String profile(Model model, @PathVariable("login") String login) {
        @Nullable final User user = userService.findOneByLogin(login);
        if (user == null) return "redirect:/";
        model.addAttribute("user", user);
        List<Project> projectList = user.getProjectList();
        model.addAttribute("projects", projectList);
        return "profile";
    }

}
