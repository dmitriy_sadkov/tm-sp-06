package ru.sadkov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@UtilityClass
public final class RandomUtil {
    @NotNull
    public static String UUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

}
