package ru.sadkov.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sadkov.tm.dto.ProjectDTO;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.User;
import ru.sadkov.tm.model.enumerate.Role;
import ru.sadkov.tm.model.enumerate.Status;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.repository.UserRepository;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.util.RandomUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService extends AbstractService implements IProjectService {


    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Autowired
    private UserRepository userRepository;


    @Override
    public Project findOne(@NotNull final String projectId) {
        if (projectRepository.findById(projectId).isPresent()) return projectRepository.findById(projectId).get();
        return null;
    }

    @Override
    public void remove(@NotNull final String projectId) {
        projectRepository.deleteById(projectId);
    }

    @Nullable
    public String findProjectIdByName(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        @Nullable final Project project = projectRepository.findOneByName(projectName);
        if (project == null) return null;
        return project.getId();
    }


    @Transactional
    @Override
    public boolean persist(@NotNull String id, @Nullable String projectName, @Nullable String description) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        if (projectRepository.existsByName(projectName)) return false;
        @NotNull final Project project = new Project(projectName, description, RandomUtil.UUID());
        project.setUser(userRepository.findById(id).get());
        projectRepository.save(project);
        return true;
    }

    @Transactional
    public void removeByName(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        @Nullable final String projectId = findProjectIdByName(projectName);
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.deleteByName(projectName);
    }

    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void update(@Nullable final String id, @Nullable final String newName, @Nullable final String description) {
        if (id == null || newName == null || id.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        projectRepository.update(id, description, newName);
    }

    public @Nullable Project findOneByName(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        @Nullable final String projectId = findProjectIdByName(projectName);
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.findOneByName(projectName);
    }

    @Override
    public @Nullable List<Project> findProjectsByPart(@Nullable final String part) {
        if (part == null || part.isEmpty()) return null;
        return projectRepository.findProjectsByPart(part);
    }

    @Override
    public @Nullable List<Project> findProjectsByStatus(@Nullable final Status status) {
        if (status == null) return null;
        return projectRepository.findProjectsByStatus(status);
    }

    @Override
    @Nullable
    @Transactional
    public String startProject(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        @NotNull final Date startDate = new Date();
        projectRepository.startProject(Status.PROCESS, projectName, startDate);
        return simpleDateFormat.format(startDate);
    }

    @Override
    @Transactional
    public @Nullable String endProject(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        @NotNull final Date endDate = new Date();
        projectRepository.endProject(Status.DONE, projectName, endDate);
        return simpleDateFormat.format(endDate);
    }

    @Override
    public @NotNull List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        clear();
        for (@NotNull final Project project : projects) {
            persist(project);
        }
    }

    @Override
    public List<Project> findProjectsByUser(@NotNull final String login) {
        @Nullable final String id = userRepository.findByLogin(login).getId();
        @Nullable List<Project> projects = projectRepository.findAllByUserId(id);
        return projects;
    }

    @Override
    @NotNull
    public ProjectDTO convert(@NotNull final Project p) {
        @NotNull final ProjectDTO result = new ProjectDTO();
        result.setId(p.getId());
        result.setName(p.getName());
        result.setDescription(p.getDescription());
        result.setUserId(p.getUser().getId());
        result.setDateBegin(p.getDateBegin());
        result.setDateCreate(p.getDateCreate());
        result.setDateEnd(p.getDateEnd());
        result.setStatus(p.getStatus());
        return result;
    }

    @Override
    @Transactional
    public void saveProjectFromDTO(@NotNull final ProjectDTO projectDTO) {
        System.out.println("save project from dto");
        Project project = new Project();
        project.setUser(userRepository.findById(projectDTO.getUserId()).get());
        project.setId(projectDTO.getId());
        project.setDescription(projectDTO.getDescription());
        project.setName(projectDTO.getName());
        project.setDateBegin(projectDTO.getDateBegin());
        project.setDateCreate(projectDTO.getDateCreate());
        project.setDateEnd(projectDTO.getDateEnd());
        project.setStatus(projectDTO.getStatus());
        project.setTasks(new ArrayList<>());
        projectRepository.save(project);
    }
}
